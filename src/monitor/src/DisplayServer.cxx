#include "monitor/DisplayServer.h"
#include "TClass.h"

#include <algorithm>
#include <csignal>
#include <cstring>
#include <exception>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <stdexcept>
#include "TRootSniffer.h"
#include "TFile.h"
#include "TFolder.h"
#include "TROOT.h"


#include <filesystem>
namespace fs = std::filesystem;

#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"

volatile sig_atomic_t sig_caught = 0;

void handle_sig(int signum) {
  /* in case we registered this handler for multiple signals */
  if (signum == SIGINT) {
    sig_caught = 1;
  }
  if (signum == SIGTERM) {
    sig_caught = 2;
  }
  if (signum == SIGABRT) {
    sig_caught = 3;
  }
}

namespace hallc {

  DisplayServer::DisplayServer(int http_port, std::string host, int sock_srv_port)
      : _http_port(http_port), _host(host), _sock_srv_port(sock_srv_port),
        _server(std::make_shared<THttpServer>((std::string("http:") + host + ":" +
                                               std::to_string(http_port) +
                                               std::string("?top=replay&thrds=2;rw"))
                                                  .c_str())) {
    spdlog::info("Creating display server at http://{}:{}",host,http_port);
    spdlog::info("Socket server listening on port {}",sock_srv_port);
    if( !(_server->IsAnyEngine()) ) {
      spdlog::error("Failed to start http server.");
      std::exit(-1);
    }
    //_server->SetDefaultPage("online.htm");
    //_server->SetDefaultPage("draw.htm");
    _server->SetCors();
    _mon = new TMonitor;
  }

  int DisplayServer::SaveTopFolder(std::string fname){
    spdlog::info("Saving top folder");
    if (_server->GetSniffer()) {
      spdlog::info("have sniffer");
      if (_server->GetSniffer()->GetTopFolder()) {
        spdlog::info("have top folder");
        if (_server->GetSniffer()->GetTopFolder()->GetListOfFolders()) {
          spdlog::info("have list of folders");
          if (_server->GetSniffer()->GetTopFolder()->GetListOfFolders()->GetEntries() > 0) {
            _server->GetSniffer()->GetTopFolder()->SaveAs(fname.c_str());
          }
        }
      } else {
        spdlog::warn("no top folder available");
      }
    }
    return 0;
  }

  int DisplayServer::LoadTopFolder(std::string fname, bool file_must_exist){
    fs::path in_path = fname;
    if (!fs::exists(in_path)) {
      if (file_must_exist) {
        spdlog::error("File : {} not found.", fname);
        std::exit(EXIT_FAILURE);
      } else {
        spdlog::warn("File : {} not found.", fname);
        return 0;
      }
    }
    TFile    infile(fname.c_str(), "READ");
    TFolder* top_folder = (TFolder*)gROOT->FindObject("http");
    if (!top_folder) {
      spdlog::warn("http folder not found in file, {}", fname);
    }
    _server->GetSniffer()->GetTopFolder(true)->GetListOfFolders()->AddAll(
        (TCollection*)top_folder->GetListOfFolders()->Clone());
    return 0;
  }

  int DisplayServer::StartSocketServer(int port) {
    _sock_srv_port = port;
    _ss            = new TServerSocket(_sock_srv_port, kTRUE);
    _mon->Add(_ss);
    return port;
  }

  void DisplayServer::Run() {
    TMessage* mess = nullptr;

    void (*prev_handler)(int);
    prev_handler = signal(SIGINT, handle_sig);

    // Loop until an interrupt (ctrl-c) is issued
    while (1) {
      if (sig_caught) {
        break;
      }
      _mon->ResetInterrupt();
      TSocket* s = _mon->Select(1000);

      if (s == (TSocket*)-1) {
        spdlog::trace("monitor timeout");
        continue;
      }

      if (s->IsA() == TServerSocket::Class()) {
        spdlog::info("Server socket connected");
        TSocket* s0 = ((TServerSocket*)s)->Accept();
        // std::cout << " error code : " << s0->GetErrorCode() << "\n";
        _running_sockets.push_back(s0);
        _mon->Add(s0);
        s0->Send("go");
        continue;
      }

      auto status = s->Recv(mess);
      // std::cout << " mess recv done\n";

      if (status <= 0) {
        spdlog::info("socket finished or disconnected somehow. Removing socket.");
        // The socket finished or disconnected somehow...
        // std::cout << " removing socket " << s << "\n";
        RemoveSocket(s);
        _mon->Remove(s);
        continue;
      }

      if (mess->What() == kMESS_STRING) {
        // std::cout << "message  string\n";
        char str[64];
        mess->ReadString(str, 64);
        auto res = std::find(std::begin(_running_sockets), std::end(_running_sockets), s);
        auto idx = std::distance(std::begin(_running_sockets), res);
        //printf("Client %d: %s\n", idx, str);
        spdlog::info("Client {}: {}",idx,str);
      } else if (mess->What() == kMESS_OBJECT) {
        auto res = std::find(std::begin(_running_sockets), std::end(_running_sockets), s);
        auto idx = std::distance(std::begin(_running_sockets), res);
        spdlog::info("Object message received on socket {}: {}",idx, mess->GetClass()->GetName());

        // std::cout << "message  object\n";
        // printf("got object of class: %s\n", mess->GetClass()->GetName());
        auto dd = (display::DisplayData*)mess->ReadObject(mess->GetClass());
        //
        Update(s, dd);
      } else {
        spdlog::error("Unexpected message!");
      }
      delete mess;
    }

    signal(SIGINT, prev_handler);
    Shutdown();
    // delete mess;
  }

  void DisplayServer::Update(TSocket* s, display::DisplayData* dd) {
    if (_connected_clients.count(s) == 0) {
      _connected_clients[s] = dd;
      for (auto& [i, data] : dd->_plot_map) {
        std::string name = dd->GetFolder() + data->_folder_name;
        auto sub_folder = _server->GetSniffer()->FindTObjectInHierarchy(name.c_str());
        if (sub_folder) {
          auto tobj = (TCanvas*)sub_folder->FindObject(data->_canvas->GetName());
          if (tobj) {
            spdlog::info("found object {} ", tobj->GetName());
            // if (!strcmp(tobj->GetName(), data->_canvas->GetName())) {
            spdlog::info("unregistering object in {} ", name);
            _server->Unregister(tobj);
          }
        }
        // std::cout << "hiding : " <<  name << "\n";
        _server->Register(name.c_str(), data->_canvas);
        for (auto h1 : data->_hists1) {
          _server->Register(name.c_str(), h1);
        }
        for(auto h2 : data->_hists2) {
          _server->Register(name.c_str(), h2);
        }

        //_server->Hide(name.c_str(), false);
      }
    } else {
      for (auto& [i, data] : dd->_plot_map) {
        _connected_clients[s]->_plot_map[i]->Replace((dd->_plot_map)[i]);
        // std::string name = std::string("/") + std::to_string(dd->_run_number) + "/";
        //_server->Register(name.c_str(), data->_canvas);
      }
    }
  }

  void DisplayServer::RemoveSocket(TSocket* s) {
    if (_connected_clients.count(s) != 0) {
      auto dd = _connected_clients[s];
      // std::string name = dd->GetFolder();// + data->_folder_name;
      // std::string name = std::string("/") + std::to_string(dd->_run_number);// +
      // data->_folder_name;
      for (auto& [i, data] : dd->_plot_map) {
        std::string name = dd->GetFolder() + data->_folder_name;

        if(!(data->_persist) ){
          _server->Unregister(data->_canvas);
          //std::cout << "hiding : " << name << "\n";
          spdlog::info("unregistering object: {}",name);
        }
        //_server->Hide(name.c_str());
        auto iter = _connected_clients.find(s);
        if (iter != std::end(_connected_clients)) {
          spdlog::info("removing connecdted client");
          _connected_clients.erase(iter);
        }

        if(!(data->_persist) ){
          spdlog::info("removing client data too.");
          for (auto& h1 : data->_hists1) {
            delete h1;
          }
          for (auto& h2 : data->_hists2) {
            delete h2;
          }
          for (auto& h3 : data->_hists3) {
            delete h3;
          }
          for (auto&& g1 : data->_graphs1) {
            delete g1;
          }
        }
        // std::string name = std::string("/") + std::to_string(dd->_run_number) + "/";
        //_server->Register(name.c_str(), data->_canvas);
      }
    }
  }

  void DisplayServer::Shutdown() {
    //TFolder* f = _server->GetSniffer()->GetTopFolder();
    //if(f) {
    //  f->SaveAs("test.root");
    //}
  }

  //_______________________________________________________________________

  // void EventDisplayServer::Update(TSocket* s, display::DisplayData* dd) {
  //  if( _connected_clients.count(s) == 0) {
  //    _connected_clients[s] = dd;
  //    for (auto& [i, data] : dd->_plot_map) {
  //      //std::string name = std::string("/") + std::to_string(dd->_run_number) +
  //      data->_folder_name; std::string name = dd->GetFolder() + data->_folder_name;
  //      _server->Register(name.c_str(), data->_canvas);
  //      std::cout << "unhiding : " <<  name << "\n";
  //      _server->Hide(name.c_str(), false);
  //    }
  //  } else {
  //    for (auto& [i, data] : dd->_plot_map) {
  //      _connected_clients[s]->_plot_map[i]->Replace((dd->_plot_map)[i]);
  //      //std::string name = std::string("/") + std::to_string(dd->_run_number) + "/";
  //      //_server->Register(name.c_str(), data->_canvas);
  //    }
  //  }
  //}

  // void EventDisplayServer::RemoveSocket(TSocket* s) {
  //  if( _connected_clients.count(s) != 0) {
  //    auto        dd   = _connected_clients[s];
  //    //std::string name = dd->GetFolder();
  //    // std::string name = std::string("/monitoring/") + std::to_string(dd->_run_number);// +
  //    // data->_folder_name;
  //    for (auto& [i, data] : dd->_plot_map) {
  //      std::string name = dd->GetFolder() + data->_folder_name;
  //      _server->Unregister(data->_canvas);
  //      std::cout << "hiding : " <<  name << "\n";
  //      _server->Hide(name.c_str());
  //      auto iter = _connected_clients.find(s);
  //      if(iter != std::end(_connected_clients) ){
  //        std::cout << "erasing \n";
  //        _connected_clients.erase(iter);
  //      }
  //      for(auto& h1 : data->_hists1 ) {
  //        delete h1;
  //      }
  //      for(auto& h2 : data->_hists2 ) {
  //        delete h2;
  //      }
  //      for(auto& h3 : data->_hists3 ) {
  //        delete h3;
  //      }
  //      //std::string name = std::string("/") + std::to_string(dd->_run_number) + "/";
  //      //_server->Register(name.c_str(), data->_canvas);
  //    }
  //  }
  //}

  ////_______________________________________________________________________
  //
  // void MonitorDisplayServer::Update(TSocket* s, display::DisplayData* dd) {
  //  if( _connected_clients.count(s) == 0) {
  //    _connected_clients[s] = dd;
  //    for (auto& [i, data] : dd->_plot_map) {
  //      std::string name = dd->GetFolder() + data->_folder_name;
  //      _server->Register(name.c_str(), data->_canvas);
  //      std::cout << "unhiding : " <<  name << "\n";
  //      _server->Hide(name.c_str(), false);
  //    }
  //  } else {
  //    for (auto& [i, data] : dd->_plot_map) {
  //      _connected_clients[s]->_plot_map[i]->Replace((dd->_plot_map)[i]);
  //    }
  //  }
  //}

  // void MonitorDisplayServer::RemoveSocket(TSocket* s) {
  //  if( _connected_clients.count(s) != 0) {
  //    auto        dd   = _connected_clients[s];
  //    //std::string name = dd->GetFolder();
  //    // std::string name = std::string("/monitoring/") + std::to_string(dd->_run_number);// +
  //    // data->_folder_name;
  //    for (auto& [i, data] : dd->_plot_map) {
  //      std::string name = dd->GetFolder() + data->_folder_name;
  //      _server->Unregister(data->_canvas);
  //      std::cout << "hiding : " <<  name << "\n";
  //      _server->Hide(name.c_str());
  //      auto iter = _connected_clients.find(s);
  //      if(iter != std::end(_connected_clients) ){
  //        std::cout << "erasing \n";
  //        _connected_clients.erase(iter);
  //      }
  //      for(auto& h1 : data->_hists1 ) {
  //        delete h1;
  //      }
  //      for(auto& h2 : data->_hists2 ) {
  //        delete h2;
  //      }
  //      for(auto& h3 : data->_hists3 ) {
  //        delete h3;
  //      }
  //      //std::string name = std::string("/") + std::to_string(dd->_run_number) + "/";
  //      //_server->Register(name.c_str(), data->_canvas);
  //    }
  //  }
  //}

} // namespace hallc

