#include "monitor/DisplayPlots.h"

#include "cppitertools/zip.hpp"

namespace hallc {
  namespace display {

    /** Merge the plot data.
     */
    void PlotData::Merge(PlotData* d) {
      for (auto&& [h1, h2] : iter::zip(_hists1, d->_hists1)) {
        (*h1) = (*h2);
      }
      for (auto&& [h1, h2] : iter::zip(_hists2, d->_hists2)) {
        (*h1) = (*h2);
      }
      for (auto&& [h1, h2] : iter::zip(_hists3, d->_hists3)) {
        (*h1) = (*h2);
      }
      for (auto&& [h1, h2] : iter::zip(_graphs1, d->_graphs1)) {
        (*h1) = (*h2);
      }
    }

    /** Replace the old data with new plot data.
     *
     */
    void PlotData::Replace(PlotData* d) {
      for (auto&& [h1, h2] : iter::zip(_hists1, d->_hists1)) {
        (*h1) = (*h2);
      }
      for (auto&& [h1, h2] : iter::zip(_hists2, d->_hists2)) {
        (*h1) = (*h2);
      }
      for (auto&& [h1, h2] : iter::zip(_hists3, d->_hists3)) {
        (*h1) = (*h2);
      }
      for (auto&& [h1, h2] : iter::zip(_graphs1, d->_graphs1)) {
        (*h1) = (*h2);
      }
    }

  } // namespace display
} // namespace hallc
