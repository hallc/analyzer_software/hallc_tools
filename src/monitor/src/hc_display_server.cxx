//#include <cmath>
//#include <iostream>
//#include <iomanip>
//#include <cstdlib>
//#include <array>
//#include <stdlib.h>
//#ifdef __cpp_lib_filesystem
#include <filesystem>
namespace fs = std::filesystem;
//#else
//#include <experimental/filesystem>
//namespace fs = std::experimental::filesystem;
//#endif
//#if !defined(__CLING__)
//#include <fmt/core.h>
//#include <fmt/ostream.h>
//#endif

#include <iostream>
#include "clipp.h"
#include "monitor/DisplayServer.h"

using namespace clipp;
using std::cout;
using std::string;


int main(int argc, char* argv[]) {

  int    http_port          = 8090;
  string http_host          = "127.0.0.1";
  int    socket_server_port = 9090;
  string init_file          = "top_folder.root";
  string save_file          = "top_folder.root";
  bool   help               = false;

  auto cli =
      ((option("-p", "--port") & value("http_port", http_port)) %
           "port to which the http serve attaches. Default: 8090 ",
       (option("-H", "--host") & value("http_host", http_host)) %
           "Http server host name or IP address. Default: 127.0.0.1",
       (option("-P", "--socket-server-port") & value("socket_server_port", socket_server_port)) %
           "Port which the socket server attaches. Default: 9090",
       (option("-f", "--file") & value("init_file", init_file)) %
           "File used to initialize the top folder objects. Default: top_folder.root",
       (option("-o", "--out-file") & value("save_file", save_file)) %
           "File used to save current server objects. Default: top_folder.root",
       option("-h","--help").set(help) % "Print help");

  // all formatting options (with their default values)
  auto clipp_format =
      doc_formatting{}
          .start_column(4) // column where usage lines and documentation starts
          .doc_column(28)  // parameter docstring start col
          .indent_size(3)  // indent of documentation lines for children of
          // a documented group .line_spacing(0)                           //number of empty lines
          // after single documentation lines .paragraph_spacing(1)                      //number of
          // empty lines before and after paragraphs .flag_separator(", ") //between flags of the
          // same parameter .param_separator(" ")                      //between parameters
          //.group_separator(" ")                      //between groups (in usage)
          //.alternative_param_separator("|")          //between alternative flags
          //.alternative_group_separator(" | ")        //between alternative groups
          //.surround_group("(", ")")                  //surround groups with these
          //.surround_alternatives("(", ")")           //surround group of alternatives with these
          //.surround_alternative_flags("", "")        //surround alternative flags with these
          //.surround_joinable("(", ")")               //surround group of joinable flags with these
          //.surround_optional("[", "]")               //surround optional parameters with these
          //.surround_repeat("", "...")                //surround repeatable parameters with these
          ////.surround_value("<", ">")                  //surround values with these
          //.empty_label("")                           //used if parameter has no flags and no label
          //.max_alternative_flags_in_usage(1)         //max. # of flags per parameter in usage
          //.max_alternative_flags_in_doc(2)           //max. # of flags per parameter in detailed
          // documentation
          .split_alternatives(true) // split usage into several lines
          // for large alternatives
          .alternatives_min_split_size(2) // min. # of parameters
      // for separate usage line
      //.merge_alternative_flags_with_common_prefix(false)  //-ab(cdxy|xy)
      // instead of -abcdxy|-abxy
      ////.merge_joinable_flags_with_common_prefix(true)     //-abc instead of -a -b -c
      ;

  assert( cli.flags_are_prefix_free() );
  auto result = parse(argc, argv, cli);

  auto doc_filter = param_filter{}.prefix("--");

  //if (help) {
  //  cout << "\033[1mHall C Display Server\033[0m\n";
  //  cout << "Usage:\n" << usage_lines(cli, "hcspec", clipp_format)
  //     << "\nOptions:\n" << documentation(cli, clipp_format,doc_filter) << '\n';
  //  std::exit(0);
  //} 
  //else if (opts.use_help == 2) {
  if (help) {
    cout << make_man_page(cli, argv[0])
                .prepend_section("DESCRIPTION" , "              things don't have to be difficult.")
                .prepend_section("NAME"        , "              \033[1mHall C Display Sever\033[0m")
                .append_section("LICENSE"      , "              GPL3")
                .append_section("EXAMPLES"      , 
                                "\n"
                                "    hc_display_server -p 8080\n"
                                "\n"
                                );
    std::exit(0);
  }

  hallc::DisplayServer srv(http_port, http_host, socket_server_port);
  srv.LoadTopFolder(init_file);
  srv.StartSocketServer(socket_server_port);
  srv.Run();
  srv.SaveTopFolder(save_file);
}

