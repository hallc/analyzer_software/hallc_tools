# HallC Tools

## Overview

A collection of libraries and executables for running and analyzing experiments in Hall C.

## Dependencies

* `hcana` and `analyzer`
    * https://eicweb.phy.anl.gov/jlab/hallc/analyzer_software/analyzer
    * https://eicweb.phy.anl.gov/jlab/hallc/analyzer_software/hcana


