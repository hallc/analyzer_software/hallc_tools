R__LOAD_LIBRARY(libScandalizer.so)
#include "monitor/DisplayServer.h"

void test_monitor(){

  auto ddisplay = new hallc::MonitoringDisplay(); // Use default socket server host/port
  ddisplay->SetRootFolder("/derp_test/");

  gRandom->SetSeed();

  auto testdisp = ddisplay->CreateDisplayPlot(
      "derp/test", "hist1",
      [&](hallc::DisplayPlot& plt) {
        auto c = plt.SetCanvas(new TCanvas(plt.GetName().c_str(), plt.GetName().c_str()));
        c->Divide(2,1);
        //plt.SetPersist();
        auto form1 = new TFormula("form1","abs(sin(x)/x)");
        auto sqroot = new TF1("sqroot","x*gaus(0) + [3]*form1",0,10);
        sqroot->SetParameters(10,4,1,20);
        sqroot->SetLineColor(4);
        sqroot->SetLineWidth(6);
        c->cd(1);
        sqroot->Draw();
        //plt._plot_data._hists1.push_back((TH1D*)sqroot->GetHistogram()->Clone());
        auto lfunction = new TPaveLabel(5,39,9.8,46,"The sqroot function");
        lfunction->Draw();

        c->cd(2);
        auto h1f = new TH1F("h1f","Test random numbers",200,0,10);
        plt._plot_data._hists1.push_back(h1f);
        h1f->SetFillColor(45);
        h1f->FillRandom("sqroot",10000);
        h1f->Draw();

        c->BuildLegend();
        return 0;
      },
      [](hallc::DisplayPlot& plt) { return 0; });

  ddisplay->InitAll();
  ddisplay->UpdateAll();

}

