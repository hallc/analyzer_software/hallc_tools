Tutorials
=========

## Display server

```
$ hc_display_server  -h
NAME
              Hall C Display Sever

DESCRIPTION
              things don't have to be difficult.

SYNOPSIS
        hc_display_server [-p <http_port>] [-H <http_host>] [-P <socket_server_port>] [-f
                          <init_file>] [-o <save_file>] [-h]

OPTIONS
        -p, --port <http_port>
                    port to which the http serve attaches. Default: 8090 

        -H, --host <http_host>
                    Http server host name or IP address. Default: 127.0.0.1

        -P, --socket-server-port <socket_server_port>
                    Port which the socket server attaches. Default: 9090

        -f, --file <init_file>
                    File used to initialize the top folder objects. Default: top_folder.root

        -o, --out-file <save_file>
                    File used to save current server objects. Default: top_folder.root

        -h, --help  Print help

LICENSE
              GPL3

EXAMPLES

    hc_display_server -p 8080

```

## `test_monitor.cxx`

```
root -b -q test_monitor.cxx
```

